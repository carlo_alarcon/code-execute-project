rem docker build . -t ubuntu-c-compiler -f Dockerfile
docker run -dit --name c-compiler-container --mount type=bind,source=D:\jalasoft-BC-carlo\project\CodeExecutionService\tmpResults,target=/develop ubuntu-c-compiler
docker cp D:\jalasoft-BC-carlo\project\CodeExecutionService\tmpResults\Main.c c-compiler-container:/develop/Main.c
docker exec c-compiler-container ./compile.sh
docker rm --force c-compiler-container