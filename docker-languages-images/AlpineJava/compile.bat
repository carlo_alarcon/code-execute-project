rem docker build . -t alpine-java-compiler -f Dockerfile
docker run -dit --name java-compiler-container --mount type=bind,source=D:\jalasoft-BC-carlo\project\CodeExecutionService\tmpResults,target=/develop alpine-java-compiler
docker cp D:\jalasoft-BC-carlo\project\CodeExecutionService\tmpResults\Main.java java-compiler-container:/develop/Main.java
docker exec java-compiler-container ./compile.sh
docker rm --force java-compiler-container
