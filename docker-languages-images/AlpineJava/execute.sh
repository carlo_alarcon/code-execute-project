#!bin/bash
mkdir -p /develop/execution
touch /develop/execution/log.txt
touch /develop/execution/error.txt
touch /develop/execution/exitcode.txt
cd develop
if test -f "Main.class"; then
	OUT=$(java Main)
	EC=$?
	if [[ $EC == 0 ]] 
	then	 
		echo $OUT > /develop/execution/log.txt
	else
		echo "Error during execution" > /develop/execution/error.txt
	fi
	echo $EC > /develop/execution/exitcode.txt
fi
