#!bin/bash
if test -f "/develop/output/log.txt"; then
	> /develop/output/log.txt
fi
if test -f "/develop/output/error.txt"; then
	> /develop/output/error.txt
fi
if test -f "/develop/output/exitcode.txt"; then
	> /develop/output/exitcode.txt
fi
OUT=$(ruby /develop/rubyProgram.rb)
EC=$?
if [[ $EC == 0 ]] 
then	 
	echo $OUT > /develop/output/log.txt
else
	ruby /develop/rubyProgram.rb 2>&1 |tee -a /develop/output/error.txt
	echo "Fail"
fi
echo $EC > /develop/output/exitcode.txt
