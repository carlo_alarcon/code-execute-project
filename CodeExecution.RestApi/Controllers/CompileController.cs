﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using CodeExecution.Domain;
using Microsoft.Extensions.Configuration;
using CodeExecution.Service;

namespace CodeExecution.RestApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CompileController : ControllerBase
    {
        private readonly ICodeExecutionService _codeExecutionService;
        public CompileController(ICodeExecutionService codeExecutionService)
        {
            _codeExecutionService = codeExecutionService;
        }
        [HttpPost]
        public IActionResult CompileCode(CodeInformation codeInformation)
        {
            return Ok(_codeExecutionService.Execute(codeInformation));
        }
    }
}
