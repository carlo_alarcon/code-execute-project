﻿using System;

namespace CodeExecution.Domain
{
    public class CodeInformation
    {
        public string Language { get; set; }
        public string Program { get; set; }
    }
}
